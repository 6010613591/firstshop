Rails.application.routes.draw do
  resources :products
  root to:'products#index'
  
  get 'static_pages/home'
  get 'static_pages/help'
  get 'static_pages/about'
  get 'layouts/login'
end
