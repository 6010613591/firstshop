class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :product
      t.string :shop1
      t.decimal :price1, precision: 7, scale: 2
      t.string :shop2
      t.decimal :price2, precision: 7, scale: 2
      t.string :shop3
      t.decimal :price3, precision: 7, scale: 2

      t.timestamps
    end
  end
end
