json.extract! product, :id, :product, :shop1, :price1, :shop2, :price2, :shop3, :price3, :created_at, :updated_at
json.url product_url(product, format: :json)
