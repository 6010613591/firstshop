require "application_system_test_case"

class ProductsTest < ApplicationSystemTestCase
  setup do
    @product = products(:one)
  end

  test "visiting the index" do
    visit products_url
    assert_selector "h1", text: "Products"
  end

  test "creating a Product" do
    visit products_url
    click_on "New Product"

    fill_in "Price1", with: @product.price1
    fill_in "Price2", with: @product.price2
    fill_in "Price3", with: @product.price3
    fill_in "Product", with: @product.product
    fill_in "Shop1", with: @product.shop1
    fill_in "Shop2", with: @product.shop2
    fill_in "Shop3", with: @product.shop3
    click_on "Create Product"

    assert_text "Product was successfully created"
    click_on "Back"
  end

  test "updating a Product" do
    visit products_url
    click_on "Edit", match: :first

    fill_in "Price1", with: @product.price1
    fill_in "Price2", with: @product.price2
    fill_in "Price3", with: @product.price3
    fill_in "Product", with: @product.product
    fill_in "Shop1", with: @product.shop1
    fill_in "Shop2", with: @product.shop2
    fill_in "Shop3", with: @product.shop3
    click_on "Update Product"

    assert_text "Product was successfully updated"
    click_on "Back"
  end

  test "destroying a Product" do
    visit products_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Product was successfully destroyed"
  end
end
