Feature: Manage products
  In order to [goal]
  [stakeholder]
  wants [behaviour]
  
  Scenario: Register new product
    Given I am on the new product page
    When I fill in "Product" with "product 1"
    And I fill in "Shop1" with "shop1 1"
    And I fill in "Price1" with "100"
    And I fill in "Shop2" with "shop2 1"
    And I fill in "Price2" with "110"
    And I fill in "Shop3" with "shop3 1"
    And I fill in "Price3" with "120"
    And I press "Create"
    Then I should see "product 1"
    And I should see "shop1 1"
    And I should see "100"
    And I should see "shop2 1"
    And I should see "110"
    And I should see "shop3 1"
    And I should see "120"

  Scenario: Delete product
    Given the following products:
      |product|shop1|price1|shop2|price2|shop3|price3|
      |product 1|shop1 1|price1 1|shop2 1|price2 1|shop3 1|price3 1|
      |product 2|shop1 2|price1 2|shop2 2|price2 2|shop3 2|price3 2|
      |product 3|shop1 3|price1 3|shop2 3|price2 3|shop3 3|price3 3|
      |product 4|shop1 4|price1 4|shop2 4|price2 4|shop3 4|price3 4|
    When I delete the 3rd product
    Then I should see the following products:
      |Product|Shop1|Price1|Shop2|Price2|Shop3|Price3|
      |product 1|shop1 1|price1 1|shop2 1|price2 1|shop3 1|price3 1|
      |product 2|shop1 2|price1 2|shop2 2|price2 2|shop3 2|price3 2|
      |product 4|shop1 4|price1 4|shop2 4|price2 4|shop3 4|price3 4|
